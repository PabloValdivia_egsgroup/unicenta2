//    Openbravo POS is a point of sales application designed for touch screens.
//    http://www.openbravo.com/product/pos
//    Copyright (c) 2007 openTrends Solucions i Sistemes, S.L
//    Modified by Openbravo SL on March 22, 2007
//    These modifications are copyright Openbravo SL
//    Author/s: A. Romero
//    You may contact Openbravo SL at: http://www.openbravo.com
//
//		Contributor: Redhuan D. Oon - ActiveMQ XML read from MClient.readmessage()
//		Please refer to notes at http://red1.org/adempiere/viewtopic.php?f=29&t=1356
//
//    This file is part of Openbravo POS.
//
//    Openbravo POS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Openbravo POS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Openbravo POS.  If not, see <http://www.gnu.org/licenses/>.

package com.openbravo.pos.erp.sync;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.UUID;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.rpc.ServiceException;

import org.compiere.model.MProduct;
import org.compiere.model.MProductPrice;
import org.compiere.model.MBPartner;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.openbravo.basic.BasicException;
import com.openbravo.data.gui.MessageInf;
import com.openbravo.data.loader.ImageUtils;
import com.openbravo.pos.customers.CustomerInfoExt;
import com.openbravo.pos.forms.AppLocal;
import com.openbravo.pos.forms.DataLogicSales;
import com.openbravo.pos.forms.DataLogicSystem;
import com.openbravo.pos.forms.ProcessAction;
import com.openbravo.pos.inventory.MovementReason;
import com.openbravo.pos.inventory.TaxCategoryInfo;
import com.openbravo.pos.ticket.CategoryInfo;
import com.openbravo.pos.ticket.ProductInfoExt;
import com.openbravo.pos.ticket.TaxInfo;
import com.openbravo.pos.erp.customers.Customer;
import com.openbravo.pos.erp.customers.Location;
import com.openbravo.pos.erp.externalsales.Category;
import com.openbravo.pos.erp.externalsales.Product;
import com.openbravo.pos.erp.externalsales.ProductPlus;
import com.openbravo.pos.erp.externalsales.Tax;
import java.net.URLDecoder;


/*
 * @contributor Sergio Oropeza - Double Click Sistemas - Venezuela - soropeza@dcs.net.ve, info@dcs.net.ve
 */


public class ProductsQueueSync implements ProcessAction {
        
    private DataLogicSystem dlsystem;
    private DataLogicIntegration dlintegration;
    private DataLogicSales dlsales;
    private String warehouse;
    private ExternalSalesHelper externalsales;
	private String productsXML  = "";
	private String customersXML = "";
    
    /** Creates a new instance of ProductsSync */
    public ProductsQueueSync(DataLogicSystem dlsystem, DataLogicIntegration dlintegration, DataLogicSales dlsales, String warehouse) {
        this.dlsystem = dlsystem;
        this.dlintegration = dlintegration;
        this.dlsales = dlsales;
        this.warehouse = warehouse;
        externalsales = null;
    }
    
    public MessageInf execute() throws BasicException {
        
        try {
        
            if (externalsales == null) {
                externalsales = new ExternalSalesHelper(dlsystem);
            }
            
            //red1 using XML string from ActiveMQ -- automatically check either path (so do not use same if u want to synch customers also)
            //red1 convert string to models deprecated approach below
            productsXML = MQClient.receiveMessage(ExternalSalesHelper.MQueueHost, ExternalSalesHelper.MQueuePORT, ExternalSalesHelper.ProductsQueue);           
             ProductPlus[] products = importQueue2Products(productsXML);
             
             customersXML = MQClient.receiveMessage(ExternalSalesHelper.MQueueHost, ExternalSalesHelper.MQueuePORT, ExternalSalesHelper.CustomersQueue);           
             Customer[]	customers = importQueue2Customers(customersXML);
            
 //          Product[] products = externalsales.getProductsCatalog();
 //          Customer[] customers = externalsales.getCustomers();
            if (products == null || customers == null){
                throw new BasicException(AppLocal.getIntString("message.returnnull"));
            }     
            
            if (products.length > 0){
                 dlintegration.syncProductsBefore();                
                    Date now = new Date();
                    System.out.println(products.length);
                    for (Product product : products) {
                        System.out.println("Registering Product: "+product.getName());
                            if (product==null) break;
                        // Synchonization of taxcategories
                        TaxCategoryInfo tc = new TaxCategoryInfo(product.getTax().getId(), URLDecoder.decode(product.getTax().getName(),"UTF-8"));
                        dlintegration.syncTaxCategory(tc);
                        TaxCategoryInfo tcaux = dlintegration.getTaxCategoryInfoByName(tc.getName());
                        // Synchonization of taxes
                        TaxInfo t = new TaxInfo(
                                product.getTax().getId(),
                                URLDecoder.decode(product.getTax().getName(),"UTF-8"),
                                tcaux.getID(),
                                //new Date(Long.MIN_VALUE),
                                null,
                                null,
                                product.getTax().getPercentage() / 100,
                                false,
                                0);
                        dlintegration.syncTax(t);
                       
                        // Synchonization of categories
                        CategoryInfo c = new CategoryInfo(product.getCategory().getId(), URLDecoder.decode(product.getCategory().getName(),"UTF-8"), null,null,null);
                        dlintegration.syncCategory(c);
                        CategoryInfo caux = dlintegration.getCategoryInfoByName(c.getName());
                        // Synchonization of products
                        ProductInfoExt pInfoExt = new ProductInfoExt();
                        pInfoExt.setID(product.getId());
                        pInfoExt.setReference(product.getReference());
                        pInfoExt.setCode(product.getEan() == null || product.getEan().equals("") ? product.getId() : product.getEan());
                       //String auxname=product.getName().replaceAll("%(?![0-9a-fA-F]{2})", "%25");
                       // auxname=auxname.replaceAll("\\+", "%2B");
                        System.out.println(product.getName());
                        pInfoExt.setName(URLDecoder.decode(product.getName(),"UTF-8"));
                        pInfoExt.setCom(false);
                        pInfoExt.setScale(false);
                        pInfoExt.setPriceBuy(product.getPurchasePrice());
                        pInfoExt.setPriceSell(product.getListPrice());
                        pInfoExt.setCategoryID(caux.getID());
                        pInfoExt.setTaxCategoryID(tcaux.getID());
                        pInfoExt.setImage(ImageUtils.readImage(product.getImageUrl()));
                        // build html display like <html><font size=-2>MIRACLE NOIR<br> MASK</font>
                        
                        pInfoExt.setDisplay("<html><font size=-2>"+product.getName().substring(0, product.getName().length()>15?15:product.getName().length()) 
                                    + "<br>" +((product.getName().length()>15)? product.getName().substring(15, product.getName().length()>30?30:product.getName().length()):"")
                                    + "</font>");
                        dlintegration.syncProduct(pInfoExt);
                        
                        // Synchronization of stock          
                        if (product instanceof ProductPlus) {
                            
                            ProductPlus productplus = (ProductPlus) product;
                            ProductInfoExt productaux=dlsales.getProductInfoByCode(productplus.getReference());
                            //  Synchonization of locations
                            dlintegration.syncLocations(productplus.getLocation_id(),productplus.getLocation_name());
                            double diff = productplus.getQtyonhand() - dlsales.findProductStock(productplus.getLocation_id(), pInfoExt.getID(), null);
                            
			    Object[] diary = new Object[9];
                            diary[0] = UUID.randomUUID().toString();
                            diary[1] = now;
                            diary[2] = diff > 0.0 
                                    ? MovementReason.IN_MOVEMENT.getKey()
                                    : MovementReason.OUT_MOVEMENT.getKey();
                            //diary[3] = warehouse;
                            diary[3] = productplus.getLocation_id();
                            String pid=pInfoExt.getID();
                            if (productaux.getID()!=null)
                                pid=productaux.getID();
                            diary[4] = pid;
                            diary[5] = null; ///TODO find out where to get AttributeInstanceID -- red1
                            diary[6] = new Double(diff);
                            diary[7] = new Double(pInfoExt.getPriceBuy());
                            diary[8] = dlsystem.getUser();
                            dlsales.getStockDiaryInsert().exec(diary);    
                    }
                }               
                    dlintegration.syncProductsAfter();
            }           
            if (customers.length > 0 ) {              
               // dlintegration. syncCustomersBefore();
				//Add RIF to field Taxid
                for (Customer customer : customers) {    
                        System.out.println("Registering customer: "+customer.getName());
                        CustomerInfoExt cinfo = new CustomerInfoExt(customer.getId());
                        cinfo.setTaxid(fixRif(customer.getSearchKey()));
                        cinfo.setSearchkey(customer.getSearchKey());
                        cinfo.setName(customer.getName());          
                        cinfo.setNotes(customer.getDescription());
                        Location loc[]=new Location[1];
                        loc=customer.getLocations();
                        cinfo.setAddress(loc[0].getAddress1());
                        cinfo.setVisible(customer.getVisible());
                        cinfo.setMaxdebt(customer.getMaxdebt()); 
                        dlintegration.syncCustomer(cinfo);
                    }
            }
            
            if (products.length == 0 && customers.length == 0) {
                return new MessageInf(MessageInf.SGN_NOTICE, AppLocal.getIntString("message.zeroproducts"));               
            } else {
                return new MessageInf(MessageInf.SGN_SUCCESS, AppLocal.getIntString("message.syncproductsok"), AppLocal.getIntString("message.syncproductsinfo", products.length, customers.length));
            }
                
        } catch (ServiceException e) {            
            throw new BasicException(AppLocal.getIntString("message.serviceexception"), e);
        } catch (MalformedURLException e){
            throw new BasicException(AppLocal.getIntString("message.malformedurlexception"), e);
        } catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return new MessageInf(MessageInf.SGN_CAUTION, AppLocal.getIntString("message.zerocustomers"));
        
    }
    // Remove hyphens and spaces of RIF
    private String fixRif(String rif){
    String RIF = rif;
        if(rif != null){          
           RIF =  rif.replace("-","" );
        }
     return RIF.toUpperCase();
    }

private ProductPlus[] importQueue2Products(String productsXML) throws SAXException, IOException, ParserConfigurationException {
		//uncomment for testing, together with above
//		message = "<?xml version=\"1.0\" ?><entityDetail><type>MProduct</type><detail>..</detail></entityDetail>";
		if (productsXML.equals("")) 
			return new ProductPlus[0];
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(new ByteArrayInputStream(productsXML.getBytes()));
                Element docEle = doc.getDocumentElement();
                NodeList records = docEle.getElementsByTagName("detail");
		ProductPlus[] product = new ProductPlus[records.getLength()];
		
		int cnt = -1;	
	    	for(int i = 0 ; i < records.getLength();i++) {
	
	    		//Checks to disallow certain detail records...
	    	    //check if right POS Name (within the loop, as all Orgs are together)
	
	    		//check if XML type is about Products
	    	    if (!records.item(i).getFirstChild().getTextContent().equals(MProduct.Table_Name))
	    	    	continue;

                        Category cate = null; 
                        Tax newtax = null;

		    	NodeList details = records.item(i).getChildNodes();
		    	for(int j = 0 ; j < details.getLength();j++) {
		    		Node n = details.item(j);
		    		String column = n.getNodeName();
                                
                                //cate.setId("99");//will be replaced later by XML tags later
                                //cate.setName("ADTax"); //will be replaced later
                                
		    		if (column.equals("POSLocatorName")) 
		    		{
		    			//checking if right POS Name
		    			//if (!poslocator.equals(n.getTextContent()))
		    			//	break; 
                                        
                                        
                                    	cnt++;
			    		product[cnt] = new ProductPlus();                 
                                        cate = new Category();
                                        newtax = new Tax();
                                        
                                        product[cnt].setLocation_name(n.getTextContent());
		    		}
                                else if (column.equals("M_Warehouse_ID")) 
		    		{
                                    product[cnt].setLocation_id(n.getTextContent());
		    		}

                                else if (column.equals("ProductName")) 
		    			product[cnt].setName(URLDecoder.decode(n.getTextContent(),"UTF-8"));
		    		
		    		else if (column.equals(MProduct.COLUMNNAME_M_Product_Category_ID)) 
	    			{
	    				cate.setId(n.getTextContent());
                                        if (cate.getName() != null)
                                            product[cnt].setCategory(cate);
	    			}		    		

                                else if (column.equals("CategoryName")) 
	    			{
	    				cate.setName(URLDecoder.decode(n.getTextContent(),"UTF-8"));
                                        if (cate.getName() != null)
                                            product[cnt].setCategory(cate);
	    			}		
		    		
		    		else if (column.equals(MProduct.COLUMNNAME_M_Product_ID)) 
		    			product[cnt].setId(n.getTextContent());
		    		
		    		else if (column.equals(MProduct.COLUMNNAME_C_TaxCategory_ID)) 
		    			{
		    				newtax.setId(n.getTextContent());
                                                if (newtax.getName() != null)
                	    				product[cnt].setTax(newtax);
		    			}
		    		else if (column.equals("TaxName")) 
	    				{
	    					newtax.setName(n.getTextContent());
                                                if (newtax.getId() != null)
        	    					product[cnt].setTax(newtax);
    	    				}
		    		else if (column.equals("TaxRate")) 
	    				{
	    					newtax.setPercentage(Double.parseDouble(n.getTextContent()));
                                                if (newtax.getId() != null || newtax.getName() != null)
                                                    product[cnt].setTax(newtax);
	    				}
                                 // Add Reference XML file            
                                //else if (column.equals("Reference")) 
                                //        {
	    			//		product[cnt].setReference(n.getTextContent());
                                //        }
		    		
		    		else if (column.equals("QtyOnHand")) 
		    			product[cnt].setQtyonhand(Double.parseDouble(n.getTextContent()));
		    		
		    		else if (column.equals(MProductPrice.COLUMNNAME_PriceList)) 
		    			product[cnt].setListPrice(Double.parseDouble(n.getTextContent()));
		    		else if (column.equals(MProductPrice.COLUMNNAME_PriceLimit)) 
		    			product[cnt].setPurchasePrice(Double.parseDouble(n.getTextContent()));
		    		else if (column.equals(MProduct.COLUMNNAME_UPC)) 
                                        {
                                            product[cnt].setEan(n.getTextContent());
                                            // Reference
                                            product[cnt].setReference(n.getTextContent());
                                        }

		    	}
	    	}
            // need to truncate products from nulls;
	    	ProductPlus[] nettProduct = new ProductPlus[cnt+1]; 
	    	for (int i=0;i <= cnt; i++){
	    		nettProduct[i] = product[i];
	    	}	    	return nettProduct;
	}
	
	private Customer[] importQueue2Customers(String customersXML) throws ParserConfigurationException, SAXException, IOException {
	if (customersXML.equals("")) return new Customer[0];
	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
	Document doc = db.parse(new ByteArrayInputStream(customersXML.getBytes()));
        Element docEle = doc.getDocumentElement();
        NodeList records = docEle.getElementsByTagName("detail");
        Customer[] customer = new Customer[records.getLength()];
	
    	for(int i = 0 ; i < records.getLength();i++) {
    	    if (!records.item(i).getFirstChild().getTextContent().equals(MBPartner.Table_Name))
    	    	continue;
            customer[i] = new Customer();
	    NodeList details = records.item(i).getChildNodes();
	    for(int j = 0 ; j < details.getLength();j++) {
	    	Node n = details.item(j);
	    	String column = n.getNodeName();
	    	if (column.equals("CustomerName")) {
                    customer[i].setName(URLDecoder.decode(n.getTextContent(),"UTF-8"));
                }else if (column.equals(MBPartner.COLUMNNAME_Value)) 
                    customer[i].setSearchKey(n.getTextContent());
	    	else if (column.equals(MBPartner.COLUMNNAME_C_BPartner_ID)) 
                    customer[i].setId(n.getTextContent());
	    	else if (column.equals(MBPartner.COLUMNNAME_Description)) 
                    customer[i].setDescription(URLDecoder.decode(n.getTextContent(),"UTF-8"));
                else if (column.equals("Address1")){ 
                    Location[] loc= new Location[1];
                    loc[0]= new Location();
                    loc[0].setAddress1(URLDecoder.decode(n.getTextContent(),"UTF-8"));
                    customer[i].setLocations(loc);
                }else if(column.equals("IsActive")){
                    if (n.getTextContent().compareTo("Y")==0)
                        customer[i].setVisible(Boolean.TRUE);
                    else
                        customer[i].setVisible(Boolean.FALSE);
                
                }else if(column.equals("TotalOpenBalance")){
                    customer[i].setMaxdebt(Double.parseDouble(n.getTextContent()));
                }
	    }
    	}
    	return customer;
	} 
}
	
	 